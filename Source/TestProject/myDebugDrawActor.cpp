// Fill out your copyright notice in the Description page of Project Settings.


#include "myDebugDrawActor.h"

// Sets default values
AmyDebugDrawActor::AmyDebugDrawActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AmyDebugDrawActor::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle myTimerHandle;
	myParams.bReturnPhysicalMaterial = true;

	//temp array for actors on the scene 
	TArray<AActor*> TempArray;

	//array for all actors that should be ignored
	TArray<AActor*> ArrayOfActorsToIgnore;


	if (ToIgnore.Num()!=0)
	{
		for (auto& TempElem : ToIgnore)
		{
			UGameplayStatics::GetAllActorsOfClass(this, TempElem, TempArray);
			ArrayOfActorsToIgnore.Append(TempArray);
		}
	
		myParams.AddIgnoredActors(ArrayOfActorsToIgnore);
	}
	GetWorldTimerManager().SetTimer(myTimerHandle, this, &AmyDebugDrawActor::AddObjectsToFade, fadeSpeed, true);
}

// Called every frame
void AmyDebugDrawActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AmyDebugDrawActor::AddObjectsToFade()
{
	/*if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Blue, FString::FromInt(myParams.GetIgnoredActors().Num()));
	}
	*/

	FVector CameraLocation = GEngine->GetFirstLocalPlayerController(GetWorld())->PlayerCameraManager->GetCameraLocation();
	FVector CharacterLocation = GEngine->GetFirstLocalPlayerController(GetWorld())->GetPawn()->GetActorLocation();


	// create a collision 
	FCollisionShape MyColCapsule = FCollisionShape::MakeCapsule(CapsuleRadius, (CameraLocation - CharacterLocation).Size()/2);

	//Camera location
	if (GEngine)
	{
	GEngine->AddOnScreenDebugMessage(-1,1.0f, FColor::Yellow, TEXT("Camera Location: ") + CameraLocation.ToString());
	}
	//Pawn location

	if (GEngine)
	{
	GEngine->AddOnScreenDebugMessage(-1,1.0f,FColor::Yellow, TEXT("Character Location: ") + CharacterLocation.ToString());
	}

	FQuat QuaternionForCapsule = FQuat(FRotator((UKismetMathLibrary::FindLookAtRotation(CharacterLocation, CameraLocation).Pitch) - (90), UKismetMathLibrary::FindLookAtRotation(CharacterLocation, CameraLocation).Yaw, 0));

	DrawDebugCapsule(GetWorld(), (CameraLocation + CharacterLocation) / 2, MyColCapsule.GetCapsuleHalfHeight(), MyColCapsule.GetCapsuleRadius(), QuaternionForCapsule, FColor::Red, false, 1);
	DrawDebugCapsule(GetWorld(), (CameraLocation + CharacterLocation) / 2 + 1, MyColCapsule.GetCapsuleHalfHeight(), MyColCapsule.GetCapsuleRadius(), QuaternionForCapsule, FColor::Green, false, 1);

	bool isHit = GetWorld()->SweepMultiByChannel(OutHits, (CameraLocation + CharacterLocation) / 2, (CameraLocation + CharacterLocation) / 2 + 1, QuaternionForCapsule, ECC_WorldStatic, MyColCapsule, myParams);

	DrawDebugPoint(GetWorld(), (CameraLocation+CharacterLocation)/2, 30.0f,FColor::Green,false, 1);
	//Rotation between camera and character
	


	if (isHit)
	{
	//������ ���
		//CapsulePrimitiveArray.Empty();
		if (CapsulePrimitiveArray.Num()==0 && PrimitiveArrayToCompare.Num()==0)
		{
			for (FHitResult& Hit : OutHits)
			{
				CapsulePrimitiveArray.Add(Hit.GetComponent());
				PrimitiveArrayToCompare.Add(Hit.GetComponent());
				MaterialArrayToCompare.Add(Hit.GetComponent()->GetMaterial(0));
			}

			CapsulePrimitiveArray.Empty();

			GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Green, TEXT("ONE TIME"));
		}
		else 
		{
			// 
			for (FHitResult& Hit : OutHits)
			{
				if (!CapsulePrimitiveArray.Contains(Hit.GetComponent()))
				{
					CapsulePrimitiveArray.Add(Hit.GetComponent());
				}		
			}

			if (PrimitiveArrayToCompare.Num() == 0)
			{
				for (FHitResult& Hit : OutHits)
				{
					PrimitiveArrayToCompare.Add(Hit.GetComponent());
					MaterialArrayToCompare.Add(Hit.GetComponent()->GetMaterial(0));
				}
			}
				//������� ������ ��������
				MakeFadeOrNot(false);

				GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Purple, FString("Primitive array num: ") + FString::FromInt(PrimitiveArrayToCompare.Num()));
				GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, FString("Capsule primitive num: ") + FString::FromInt(CapsulePrimitiveArray.Num()));
		}
	}
	else
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("FALSE"));
		}
		//�������� ������� ��������� ���� ����������
		CapsulePrimitiveArray.Empty();

		MakeFadeOrNot(true);
	}
	CapsulePrimitiveArray.Empty();
}

void AmyDebugDrawActor::MakeFadeOrNot(bool AllAppeares)
{

	
	PrimitiveArrayToCompare.Append(PrimitiveArrayTemp);
	MaterialArrayToCompare.Append(MaterialArrayTemp);

	PrimitiveArrayTemp.Empty();
	MaterialArrayTemp.Empty();

	int countOfDif = 0;

	if (AllAppeares)
	{
		for (int i = 0; i < PrimitiveArrayToCompare.Num(); i++)
		{
			PrimitiveArrayToCompare[i]->SetMaterial(0,MaterialArrayToCompare[i]);
		}
		PrimitiveArrayToCompare.Empty();
		MaterialArrayToCompare.Empty();
	}	
	else 
	{
		for (int i = 0; i < PrimitiveArrayToCompare.Num(); i++)
		{
			if (CapsulePrimitiveArray.Contains(PrimitiveArrayToCompare[i]))
			{
				PrimitiveArrayTemp.Add(PrimitiveArrayToCompare[i]);
				MaterialArrayTemp.Add(MaterialArrayToCompare[i]);
				PrimitiveArrayToCompare[i]->SetMaterial(0, FadeMaterial);
			}
			else if (!CapsulePrimitiveArray.Contains(PrimitiveArrayToCompare[i]))
			{		
				PrimitiveArrayToCompare[i]->SetMaterial(0, MaterialArrayToCompare[i]);		
			}
		}
	
			PrimitiveArrayToCompare.Empty();
			MaterialArrayToCompare.Empty();
	}
	
}

