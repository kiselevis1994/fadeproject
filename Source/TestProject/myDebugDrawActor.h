// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//My Includes
#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TimerManager.h"
#include "GameFramework/Actor.h"
#include "CollisionQueryParams.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/MaterialInstance.h"
#include "UObject/UObjectArray.h"

#include "myDebugDrawActor.generated.h"

UCLASS()
class TESTPROJECT_API AmyDebugDrawActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AmyDebugDrawActor();

	

	UPROPERTY(EditAnywhere, Category = "Capsule Radius")
	float CapsuleRadius = 1.0f;

	UPROPERTY(EditAnywhere, Category = "Capsule Radius")
		float fadeSpeed = 1.0f;



	//Actors to ignore
	UPROPERTY(EditAnywhere, Category = "Array To Ignore")
	TArray<TSubclassOf<AActor>> ToIgnore;

	UPROPERTY(EditAnywhere, Category = "Fade Material")
	UMaterialInstance* FadeMaterial;


	FCollisionQueryParams myParams;

	// ������ ���������� ��������
	TArray<UPrimitiveComponent*> CapsulePrimitiveArray;
	//TArray<UMaterialInterface*> CapsuleMaterialArray;

	// ������ - �����������
	TArray<UPrimitiveComponent*> PrimitiveArrayToCompare;
	TArray<UMaterialInterface*> MaterialArrayToCompare;

	//��������� �������
	TArray<UPrimitiveComponent*> PrimitiveArrayTemp;
	TArray<UMaterialInterface*> MaterialArrayTemp;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddObjectsToFade();
	void MakeFadeOrNot(bool AllAppeares);

private:
	// create tarray for hit results
	TArray<FHitResult> OutHits;


};
